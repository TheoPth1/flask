# Deploy the app : 

1. Create a new project on Openshift and connect to it

2. Build the image
`docker build -t theopth/demoflask`

3. Push to the registry
`docker push theopth/demoflask`

*Used private registry*

Connect to private registry inside the openshift
* Project default => registry-console => connect with admin credentials
* Copy / Paste 
`docker login -p token -u unused registry.atlas-iet2.tlscontact.com` (with the valid token)
* Tag your docker image
`docker tag theopth/demoflask registry.atlas-iet2.tlscontact.com/demo/demoflask`
 
* Push
`docker push registry.atlas-iet2.tlscontact.com/demo/demoflask`
( Don't forget to change image name in the deployment template !)

4. Deploy the template
`oc apply -f openshfit/app.yml`